# QProto Processing Algorithm

QPROTO is an algorithm for rockfall analyses at a small scale on the basis of
the simplified *Cone Method* (Jaboyedoff & Labious, 2011). 

It allows to preliminarily evaluate the effects of a rockfall event along a
slope in terms of exposed areas, boulder velocity and boulder energy.

A time-independent rockfall hazard can also be defined.

QPROTO is designed by Politecnico di Torino (Marta Castelli, Monica Barbero &
Marco Grisolia), Arpa Piemonte (Rocco Pispico & Luca Lanteri) and developed by
Faunalia.

This Plugin is partially funded by Italian 2015 PRIN Project: "Innovative
monitoring and design strategies for sustainable landslide risk mitigation".
